﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.Core.EventProcessing
{
    public interface IEventProcessor
    {
        void ProcessEvent(string message);
    }
}
