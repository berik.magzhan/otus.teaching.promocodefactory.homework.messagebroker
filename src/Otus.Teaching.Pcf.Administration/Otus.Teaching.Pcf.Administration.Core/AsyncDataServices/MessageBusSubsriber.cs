﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Administration.Core.Domain.Dto;
using Otus.Teaching.Pcf.Administration.Core.EventProcessing;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.AsyncDataServices
{
    public class MessageBusSubsriber: BackgroundService
    {
        private readonly IConfiguration _configuration;
        private readonly IEventProcessor _eventProcessor;
        private IConnection _connection;
        private IModel _channel;

        public MessageBusSubsriber(IConfiguration configuration, IEventProcessor eventProcessor)
        {
            _configuration = configuration;
            _eventProcessor = eventProcessor;
            InitializeRabbitMQ();
        }

        private void InitializeRabbitMQ()
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            Console.WriteLine($"--> Trying to connect on: {factory.HostName}:{factory.Port}");

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            _channel.ExchangeDeclare(exchange: "adminNotification", type: ExchangeType.Fanout);

            _channel.QueueDeclare(queue: "adminNotify",
                                durable: false,
                                exclusive: false,
                                autoDelete: false,
                                arguments: null);
            _channel.QueueBind(queue: "adminNotify",
                                  exchange: "adminNotification",
                                  routingKey: "");

        }


        public override void Dispose()
        {
            if (_channel.IsOpen)
            {
                _channel.Close();
                _connection.Close();
            }

            base.Dispose();
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (sender, args) =>
            {
                Console.WriteLine("--> Received Message");
                var body = args.Body;
                var message = Encoding.UTF8.GetString(body.ToArray());

                _eventProcessor.ProcessEvent(message);
                Console.WriteLine($"--> Message contents: {message}");

            };

            _channel.BasicConsume(queue: "adminNotify",
                                    autoAck: true, consumer);

            return Task.CompletedTask;
        }
    }
}
