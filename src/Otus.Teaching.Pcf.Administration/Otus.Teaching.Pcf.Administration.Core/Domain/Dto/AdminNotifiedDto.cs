﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Dto
{
    public class AdminNotifiedDto
    {
        public AdminNotifiedDto(Guid? guid)
        { 
        AdminId = guid;
        }
        public Guid? AdminId { get; set; }
        public string Event { get; set; }
    }
}
