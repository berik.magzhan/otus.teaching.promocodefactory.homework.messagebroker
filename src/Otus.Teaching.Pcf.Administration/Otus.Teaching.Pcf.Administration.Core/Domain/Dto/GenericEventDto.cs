﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Dto
{
    public class GenericEventDto
    {
        public string Event { get; set; }
    }
}
