﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.AsyncDataServices
{
    public interface IMessageBusClient
    {
        void GiveNewPromoCode(GivePromoCodeToCustomerDto dto);

        void NotifyAdminAboutPromoCode(NotifyAdminDto dto);
    }
}
