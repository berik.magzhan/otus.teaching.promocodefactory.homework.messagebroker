﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.AsyncDataServices
{
    public class MessageBusClient : IMessageBusClient
    {
        private readonly IConfiguration _configuration;

        public MessageBusClient(IConfiguration configuration)
        {
            _configuration = configuration;
            
        }

        public void GiveNewPromoCode(GivePromoCodeToCustomerDto dto)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            Console.WriteLine($"--> Trying to connect on: {factory.HostName}:{factory.Port}");

            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "newPromoCodesAssigning", type: ExchangeType.Fanout);

                    Console.WriteLine("--> Connected to MessageBus");
                    channel.QueueDeclare(queue: "newPromo",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
                    channel.QueueBind(queue: "newPromo",
                                      exchange: "newPromoCodesAssigning",
                                      routingKey: "");


                    var message = JsonSerializer.Serialize(dto);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "newPromoCodesAssigning",
                        routingKey: "",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine($"--> We have sent {message}");
                }            
            }
            catch (Exception ex)
            {
                Console.WriteLine($"--> Could not connect to the Message Bus: {ex.Message}");
                Console.WriteLine($"--> InnerException: {ex.InnerException.Message}");
                throw;
            }
        }

        public void NotifyAdminAboutPromoCode(NotifyAdminDto dto)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            Console.WriteLine($"--> Trying to connect on: {factory.HostName}:{factory.Port}");

            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    Console.WriteLine("--> Connected to MessageBus");

                    channel.ExchangeDeclare(exchange: "adminNotification", type: ExchangeType.Fanout);

                    channel.QueueDeclare(queue: "adminNotify",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

                    channel.QueueBind(queue: "adminNotify",
                                      exchange: "adminNotification",
                                      routingKey: "");


                    var message = JsonSerializer.Serialize(dto);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "adminNotification",
                        routingKey: "",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine($"--> We have sent {message}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"--> Could not connect to the Message Bus: {ex.Message}");
                Console.WriteLine($"--> InnerException: {ex.InnerException.Message}");
                throw;
            }
        }

    }
}
