﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class NotifyAdminDto
    {
        public NotifyAdminDto(Guid? adminGuid)
        {
            AdminId = adminGuid;
        }

        public Guid? AdminId { get; set; }

        public string Event { get; set; }
    }
}
